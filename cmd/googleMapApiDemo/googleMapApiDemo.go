package main

import (
	"fmt"
	"net/http"

	"google-map-api-demo/internal/database"
	"google-map-api-demo/internal/env"
	"google-map-api-demo/internal/httpServer"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	// Init Environment
	env.LoadEnv()

	// Create DB Pool
	db := database.GetDBPool()
	defer db.Close()

	// Setup HTTP Router
	router := mux.NewRouter()
	httpServer.SetupHandler(router)

	// Start HTTP Server
	http.ListenAndServe(fmt.Sprintf(":%d", env.Port), router)
}
