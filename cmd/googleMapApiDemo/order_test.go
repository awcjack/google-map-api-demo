//go:build integration
// +build integration

package main_test

import (
	"encoding/json"
	"fmt"
	"google-map-api-demo/internal/order"
	"google-map-api-demo/internal/testdb"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_createOrder(t *testing.T) {
	defer func() {
		if err := testdb.CleanOrders(db); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	err := testdb.SeedOrders(db)
	if err != nil {
		t.Fatalf("Error On Seeding Orders: %v", err)
	}

	newOrdersTests := []struct {
		Name          string
		RequestBody   string
		ExpectedBody  string
		ExpectedOrder order.Order
		ExpectedCode  int
	}{
		// {
		// 	Name:          "OK",
		// 	RequestBody:   `{"origin": ["22.5040556","114.1313034"],"destination": ["22.2724959","114.1509997"]}`,
		// 	ExpectedBody:  "",
		// 	ExpectedOrder: order.Order{Id: 4, Distance: 42450, Status: "UNASSIGNED"},
		// 	ExpectedCode:  http.StatusOK,
		// },
		{
			Name:          "Wrong Input",
			RequestBody:   `{"origin": ["1"],"destination": ["1", "1"]}`,
			ExpectedBody:  `{"error": "Invalid Input"}`,
			ExpectedOrder: order.Order{},
			ExpectedCode:  http.StatusBadRequest,
		},
		{
			Name:          "Not Route",
			RequestBody:   `{"origin": ["22.5040556","114.1313034"],"destination": ["22.467704","113705165"]}`,
			ExpectedBody:  `{"error": "No Route Found"}`,
			ExpectedOrder: order.Order{},
			ExpectedCode:  http.StatusBadRequest,
		},
	}

	for _, test := range newOrdersTests {
		req, err := http.NewRequest(http.MethodPost, "/orders", strings.NewReader(test.RequestBody))
		if err != nil {
			t.Errorf("error creating request: %v", err)
		}

		w := httptest.NewRecorder()
		router.ServeHTTP(w, req)

		res := w.Result()
		if res.StatusCode != test.ExpectedCode {
			t.Fatalf("Expect %s status code is %d but received %d", test.Name, test.ExpectedCode, res.StatusCode)
		}

		defer res.Body.Close()
		if test.ExpectedBody == "" {
			responseOrder := new(order.Order)
			err = json.NewDecoder(res.Body).Decode(responseOrder)
			if err != nil {
				t.Errorf("expected error to be nil got %v", err)
			}
			// Expected Distance and received distance may differ due to google map api picking different route at different time
			if (test.ExpectedOrder.Distance != 0 && responseOrder.Distance <= 0) || responseOrder.Id != test.ExpectedOrder.Id || responseOrder.Status != test.ExpectedOrder.Status {
				t.Fatalf("Expect %s sending %v but received %v", test.Name, test.ExpectedOrder, responseOrder)
			}
		} else {
			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Errorf("expected error to be nil got %v", err)
			}
			if string(body) != test.ExpectedBody {
				t.Fatalf("Expect %s sending %s JSON response body but receive %s", test.Name, test.ExpectedBody, string(body))
			}
		}
	}
}

func Test_TakeOrder(t *testing.T) {
	defer func() {
		if err := testdb.CleanOrders(db); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	err := testdb.SeedOrders(db)
	if err != nil {
		t.Fatalf("Error On Seeding Orders: %v", err)
	}

	takeOrdersTests := []struct {
		Name         string
		RequestId    string
		ExpectedBody string
		ExpectedCode int
	}{
		{
			Name:         "OK",
			RequestId:    `1`,
			ExpectedBody: `{"status": "SUCCESS"}`,
			ExpectedCode: http.StatusOK,
		},
		{
			Name:         "Invalid Order",
			RequestId:    `100`,
			ExpectedBody: `{"error": "Invalid Order"}`,
			ExpectedCode: http.StatusBadRequest,
		},
		{
			Name:         "Invalid Order",
			RequestId:    `abc`,
			ExpectedBody: `{"error": "Id is an integer"}`,
			ExpectedCode: http.StatusBadRequest,
		},
		{
			Name:         "Taken Order",
			RequestId:    `3`,
			ExpectedBody: `{"error": "Invalid Order"}`,
			ExpectedCode: http.StatusBadRequest,
		},
	}

	for _, test := range takeOrdersTests {
		req, err := http.NewRequest(http.MethodPatch, fmt.Sprintf("/orders/%s", test.RequestId), strings.NewReader(test.RequestId))
		if err != nil {
			t.Errorf("error creating request: %v", err)
		}

		w := httptest.NewRecorder()
		router.ServeHTTP(w, req)

		res := w.Result()
		if res.StatusCode != test.ExpectedCode {
			t.Fatalf("Expect %s status code is %d but received %d", test.Name, test.ExpectedCode, res.StatusCode)
		}

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Errorf("expected error to be nil got %v", err)
		}
		if string(body) != test.ExpectedBody {
			t.Fatalf("Expect %s sending %s JSON response body but receive %s", test.Name, test.ExpectedBody, string(body))
		}
	}
}

func Test_ListOrder(t *testing.T) {
	defer func() {
		if err := testdb.CleanOrders(db); err != nil {
			t.Errorf("error truncating test database tables: %v", err)
		}
	}()

	err := testdb.SeedOrders(db)
	if err != nil {
		t.Fatalf("Error On Seeding Orders: %v", err)
	}

	listOrdersTests := []struct {
		Name           string
		RequestPage    string
		RequestLimit   string
		ExpectedBody   string
		ExpectedOrders []order.Order
		ExpectedCode   int
	}{
		{
			Name:         "OK",
			RequestPage:  `1`,
			RequestLimit: `10`,
			ExpectedBody: "",
			ExpectedOrders: []order.Order{
				{
					Id:       1,
					Distance: 100,
					Status:   "UNASSIGNED",
				},
				{
					Id:       2,
					Distance: 200,
					Status:   "UNASSIGNED",
				},
				{
					Id:       3,
					Distance: 300,
					Status:   "TAKEN",
				},
			},
			ExpectedCode: http.StatusOK,
		},
		{
			Name:         "OK",
			RequestPage:  `1`,
			RequestLimit: `1`,
			ExpectedBody: "",
			ExpectedOrders: []order.Order{
				{
					Id:       1,
					Distance: 100,
					Status:   "UNASSIGNED",
				},
			},
			ExpectedCode: http.StatusOK,
		},
		{
			Name:           "OK",
			RequestPage:    `1`,
			RequestLimit:   `0`,
			ExpectedBody:   "",
			ExpectedOrders: []order.Order{},
			ExpectedCode:   http.StatusOK,
		},
		{
			Name:           "Invalid Params",
			RequestPage:    `0`,
			RequestLimit:   `10`,
			ExpectedBody:   `{"error": "Page must be greater than 0, Limit must be greater than or equal to 0"}`,
			ExpectedOrders: []order.Order{},
			ExpectedCode:   http.StatusBadRequest,
		},
	}

	for _, test := range listOrdersTests {
		req, err := http.NewRequest(http.MethodGet, "/orders", nil)
		query := req.URL.Query()
		query.Add("page", test.RequestPage)
		query.Add("limit", test.RequestLimit)
		req.URL.RawQuery = query.Encode()
		if err != nil {
			t.Errorf("error creating request: %v", err)
		}

		w := httptest.NewRecorder()
		router.ServeHTTP(w, req)

		res := w.Result()
		if res.StatusCode != test.ExpectedCode {
			t.Fatalf("Expect %s status code is %d but received %d", test.Name, test.ExpectedCode, res.StatusCode)
		}

		defer res.Body.Close()
		if test.ExpectedBody == "" {
			var responseOrders []order.Order
			err = json.NewDecoder(res.Body).Decode(&responseOrders)
			if err != nil {
				t.Errorf("expected error to be nil got %v", err)
			}
			for i, order := range responseOrders {
				if order.Id != test.ExpectedOrders[i].Id || order.Distance != test.ExpectedOrders[i].Distance || order.Status != test.ExpectedOrders[i].Status {
					t.Fatalf("Expect %s sending %v but received %v", test.Name, test.ExpectedOrders[i], order)
				}
			}
		} else {
			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Errorf("expected error to be nil got %v", err)
			}
			if string(body) != test.ExpectedBody {
				t.Fatalf("Expect %s sending %s JSON response body but receive %s", test.Name, test.ExpectedBody, string(body))
			}
		}
	}
}
