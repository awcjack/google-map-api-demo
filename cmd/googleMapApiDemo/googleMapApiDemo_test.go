package main_test

import (
	"database/sql"
	"fmt"
	"google-map-api-demo/internal/database"
	"google-map-api-demo/internal/env"
	"google-map-api-demo/internal/httpServer"
	"google-map-api-demo/internal/testdb"
	"os"
	"testing"

	"github.com/gorilla/mux"
)

var router *mux.Router
var db *sql.DB

func TestMain(m *testing.M) {
	os.Setenv("DB_USERNAME", "root")
	os.Setenv("DB_PASSWORD", "password")
	os.Setenv("DB_HOSTNAME", "localhost")
	os.Setenv("DB_NAME", "db")
	os.Setenv("DB_PORT", "3306")
	os.Setenv("PORT", "8080")

	env.LoadEnv()

	db = database.GetDBPool()
	defer db.Close()

	database.InitDB(db)

	if err := testdb.CleanOrders(db); err != nil {
		fmt.Printf("error truncating test database tables: %v", err)
	}

	router = mux.NewRouter()

	httpServer.SetupHandler(router)

	exitCode := m.Run()

	os.Exit(exitCode)
}
