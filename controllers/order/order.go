package orderControllers

import (
	"encoding/json"
	"errors"
	"google-map-api-demo/internal/customErrors"
	"google-map-api-demo/internal/order"
	"google-map-api-demo/internal/util"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// interface for mocking
type orderInterface interface {
	CreateOrder(startLat, startLong, endLat, endLong string) (order.Order, error)
	TakeOrder(orderId int) error
	ListOrder(page, limit int, filterType string) ([]order.Order, error)
}

type orderStruct struct{}

func (orderService orderStruct) CreateOrder(startLat, startLong, endLat, endLong string) (order.Order, error) {
	return order.CreateOrder(startLat, startLong, endLat, endLong)
}

func (orderService orderStruct) TakeOrder(orderId int) error {
	return order.TakeOrder(orderId)
}

func (orderService orderStruct) ListOrder(page, limit int, filterType string) ([]order.Order, error) {
	return order.ListOrder(page, limit, filterType)
}

var orderServiceHelper orderInterface = orderStruct{}

type placeOrderRequest struct {
	Origin      []string `json:"origin"`
	Destination []string `json:"destination"`
}

// Place Order API controller
func PlaceOrder(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	request := new(placeOrderRequest)
	err := json.NewDecoder(r.Body).Decode(request)
	if err != nil {
		util.SendBadRequest(w, "")
		return
	}

	if len(request.Origin) != 2 || len(request.Destination) != 2 {
		util.SendBadRequest(w, "")
		return
	}

	order, err := orderServiceHelper.CreateOrder(request.Origin[0], request.Origin[1], request.Destination[0], request.Destination[1])
	if errors.Is(err, customErrors.ErrInvalidInput) || errors.Is(err, customErrors.ErrNoRoute) {
		util.SendBadRequest(w, err.Error())
		return
	} else if err != nil {
		util.SendInternalServerError(w, "")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(order)
}

// Take Order API controller
func TakeOrder(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	orderIdString, present := vars["orderId"]
	if !present {
		util.SendBadRequest(w, "")
		return
	}

	orderId, err := strconv.Atoi(orderIdString)
	if err != nil {
		util.SendBadRequest(w, "Id is an integer")
		return
	}

	err = orderServiceHelper.TakeOrder(orderId)
	if errors.Is(err, customErrors.ErrInvalidInput) || errors.Is(err, customErrors.ErrInvalidOrder) {
		util.SendBadRequest(w, err.Error())
		return
	} else if err != nil {
		util.SendInternalServerError(w, "")
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"status": "SUCCESS"}`))
}

// List Order API controller
func ListOrders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	query := r.URL.Query()
	pageString, present := query["page"]
	if !present {
		util.SendBadRequest(w, "Missing page query params")
		return
	}

	if len(pageString) != 1 {
		util.SendBadRequest(w, "")
		return
	}

	page, err := strconv.Atoi(pageString[0])
	if err != nil {
		util.SendBadRequest(w, "Page must be a number")
		return
	}

	limitString, present := query["limit"]
	if !present {
		util.SendBadRequest(w, "Missing limit query params")
		return
	}

	if len(limitString) != 1 {
		util.SendBadRequest(w, "")
		return
	}

	limit, err := strconv.Atoi(limitString[0])
	if err != nil {
		util.SendBadRequest(w, "Limit must be a number")
		return
	}

	if page <= 0 || limit < 0 {
		util.SendBadRequest(w, "Page must be greater than 0, Limit must be greater than or equal to 0")
		return
	}

	var filterType string
	filterTypeSlice, present := query["type"]
	if !present {
		filterType = ""
	} else {
		filterType = filterTypeSlice[0]
	}

	orders, err := orderServiceHelper.ListOrder(page, limit, filterType)
	if err != nil {
		util.SendInternalServerError(w, "")
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(orders)
}
