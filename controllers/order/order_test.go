package orderControllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"google-map-api-demo/internal/order"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/mock"
)

type orderMock struct {
	mock.Mock
}

var createOrderMock func(startLat, startLong, endLat, endLong string) (order.Order, error)
var takeOrderMock func(orderId int) error
var listOrderMock func(page, limit int, filterType string) ([]order.Order, error)

func (orderService orderMock) CreateOrder(startLat, startLong, endLat, endLong string) (order.Order, error) {
	return createOrderMock(startLat, startLong, endLat, endLong)
}

func (orderService orderMock) TakeOrder(orderId int) error {
	return takeOrderMock(orderId)
}

func (orderService orderMock) ListOrder(page, limit int, filterType string) ([]order.Order, error) {
	return listOrderMock(page, limit, filterType)
}

func TestPlaceOrder(t *testing.T) {
	orderServiceHelper = orderMock{}

	req := httptest.NewRequest(http.MethodPost, "/orders", nil)
	w := httptest.NewRecorder()
	PlaceOrder(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1"],"destination": ["1"]}`))
	w = httptest.NewRecorder()
	PlaceOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{}, customErrors.ErrNoRoute
	}
	req = httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w = httptest.NewRecorder()
	PlaceOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "No Route Found"}` {
		t.Fatalf("Expect sending No Route Found JSON response body but receive %s", string(body))
	}

	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{}, customErrors.ErrInvalidInput
	}
	req = httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w = httptest.NewRecorder()
	PlaceOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{}, errors.New("Unknown Error")
	}
	req = httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w = httptest.NewRecorder()
	PlaceOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 500 {
		t.Fatalf("Expect status code is 500 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Internal Server Error, Please try again later"}` {
		t.Fatalf("Expect sending Internal Server Error JSON response body but receive %s", string(body))
	}

	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{
			Id:       1,
			Distance: 100,
			Status:   "UNASSIGNED",
		}, nil
	}
	req = httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w = httptest.NewRecorder()
	PlaceOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Expect status code is 200 but received %d", res.StatusCode)
	}
	responseOrder := new(order.Order)
	err = json.NewDecoder(res.Body).Decode(responseOrder)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if responseOrder.Id != 1 || responseOrder.Distance != 100 || responseOrder.Status != "UNASSIGNED" {
		t.Fatalf("Expect sending Normal JSON response but received %v", responseOrder)
	}
}

// Normal use-case: Create order succeed return order
func ExampleCreateOrder() {
	orderServiceHelper = orderMock{}
	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{
			Id:       1,
			Distance: 100,
			Status:   "UNASSIGNED",
		}, nil
	}
	req := httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w := httptest.NewRecorder()
	PlaceOrder(w, req)
	res := w.Result()
	defer res.Body.Close()

	fmt.Println(res.StatusCode)
	responseOrder := new(order.Order)
	err := json.NewDecoder(res.Body).Decode(responseOrder)
	if err != nil {
		fmt.Printf("expected error to be nil got %v", err)
	}
	fmt.Println(responseOrder)
	// Output: 200
	// &{1 100 UNASSIGNED}
}

// Wrong input (origin and destination must be 2 element): Return invalid input error
func ExampleCreateOrder_wrongLatLong() {
	req := httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1"],"destination": ["1", "1"]}`))
	w := httptest.NewRecorder()
	PlaceOrder(w, req)
	res := w.Result()
	defer res.Body.Close()

	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Invalid Input"}
}

// Wrong input (Google Map API failed to guess route): Return invalid input error
func ExampleCreateOrder_noRoute() {
	orderServiceHelper = orderMock{}
	createOrderMock = func(startLat, startLong, endLat, endLong string) (order.Order, error) {
		return order.Order{}, customErrors.ErrNoRoute
	}
	req := httptest.NewRequest(http.MethodPost, "/orders", strings.NewReader(`{"origin": ["1", "1"],"destination": ["1", "1"]}`))
	w := httptest.NewRecorder()
	PlaceOrder(w, req)
	res := w.Result()
	defer res.Body.Close()

	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "No Route Found"}
}

func TestTakeOrder(t *testing.T) {
	orderServiceHelper = orderMock{}

	req := httptest.NewRequest(http.MethodPatch, "/orders", nil)
	w := httptest.NewRecorder()
	TakeOrder(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodPost, "/orders/FAKE_ORDER", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "FAKE_ORDER",
	})
	w = httptest.NewRecorder()
	TakeOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Id is an integer"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	takeOrderMock = func(orderId int) error {
		return customErrors.ErrInvalidOrder
	}
	req = httptest.NewRequest(http.MethodPatch, "/orders/1", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "1",
	})
	w = httptest.NewRecorder()
	TakeOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Order"}` {
		t.Fatalf("Expect sending Invalid Order JSON response body but receive %s", string(body))
	}

	takeOrderMock = func(orderId int) error {
		return nil
	}
	req = httptest.NewRequest(http.MethodPatch, "/orders/1", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "1",
	})
	w = httptest.NewRecorder()
	TakeOrder(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"status": "SUCCESS"}` {
		t.Fatalf("Expect sending success JSON response body but receive %s", string(body))
	}
}

// Normal use-case: picking order without taken by others
func ExampleTakeOrder() {
	orderServiceHelper = orderMock{}

	takeOrderMock = func(orderId int) error {
		return nil
	}
	req := httptest.NewRequest(http.MethodPatch, "/orders/1", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "1",
	})
	w := httptest.NewRecorder()
	TakeOrder(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 200
	// <nil>
	// {"status": "SUCCESS"}
}

// Invalid Order: picking order taken by others
func ExampleTakeOrder_taken() {
	orderServiceHelper = orderMock{}

	takeOrderMock = func(orderId int) error {
		return customErrors.ErrInvalidOrder
	}
	req := httptest.NewRequest(http.MethodPatch, "/orders/1", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "1",
	})
	w := httptest.NewRecorder()
	TakeOrder(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Invalid Order"}
}

// Invalid OrderId
func ExampleTakeOrder_wrongId() {
	orderServiceHelper = orderMock{}

	takeOrderMock = func(orderId int) error {
		return customErrors.ErrInvalidOrder
	}
	req := httptest.NewRequest(http.MethodPatch, "/orders/FAKE_ORDER", nil)
	req = mux.SetURLVars(req, map[string]string{
		"orderId": "FAKE_ORDER",
	})
	w := httptest.NewRecorder()
	TakeOrder(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Id is an integer"}
}

func TestListOrder(t *testing.T) {
	orderServiceHelper = orderMock{}

	req := httptest.NewRequest(http.MethodGet, "/orders", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Missing page query params"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&page=2", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodGet, "/orders?page=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Missing limit query params"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1&limit=10", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Invalid Input"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	req = httptest.NewRequest(http.MethodGet, "/orders?page=0&limit=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 400 {
		t.Fatalf("Expect status code is 400 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Page must be greater than 0, Limit must be greater than or equal to 0"}` {
		t.Fatalf("Expect sending Invalid Input JSON response body but receive %s", string(body))
	}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, errors.New("Unknown SQL Error")
	}
	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 500 {
		t.Fatalf("Expect status code is 500 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != `{"error": "Internal Server Error, Please try again later"}` {
		t.Fatalf("Expect sending Internal Server Error JSON response body but receive %s", string(body))
	}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Expect status code is 200 but received %d", res.StatusCode)
	}
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if string(body) != fmt.Sprintf("[]\n") {
		t.Fatalf("Expect sending Empty Array JSON response body but receive %s", string(body))
	}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{{Id: 1, Distance: 100, Status: "UNASSIGNED"}}, nil
	}
	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Expect status code is 200 but received %d", res.StatusCode)
	}
	orders := make([]order.Order, 0)
	err = json.NewDecoder(res.Body).Decode(&orders)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if orders[0].Id != 1 || orders[0].Distance != 100 || orders[0].Status != "UNASSIGNED" {
		t.Fatalf("Expect sending Normal Array JSON response body but receive %v", orders)
	}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{{Id: 1, Distance: 100, Status: "UNASSIGNED"}, {Id: 2, Distance: 200, Status: "UNASSIGNED"}, {Id: 3, Distance: 300, Status: "TAKEN"}}, nil
	}
	req = httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w = httptest.NewRecorder()
	ListOrders(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Fatalf("Expect status code is 200 but received %d", res.StatusCode)
	}
	orders = make([]order.Order, 0)
	err = json.NewDecoder(res.Body).Decode(&orders)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if (orders[0].Id != 1 || orders[0].Distance != 100 || orders[0].Status != "UNASSIGNED") || (orders[1].Id != 2 || orders[1].Distance != 200 || orders[1].Status != "UNASSIGNED") || (orders[2].Id != 3 || orders[2].Distance != 300 || orders[2].Status != "TAKEN") {
		t.Fatalf("Expect sending Normal Array JSON response body but receive %v", orders)
	}
}

// Normal usecase: return 3 rows
func ExampleListOrder() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{{Id: 1, Distance: 100, Status: "UNASSIGNED"}, {Id: 2, Distance: 200, Status: "UNASSIGNED"}, {Id: 3, Distance: 300, Status: "TAKEN"}}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	orders := make([]order.Order, 0)
	err := json.NewDecoder(res.Body).Decode(&orders)
	fmt.Println(err)
	fmt.Println(orders)
	// Output: 200
	// <nil>
	// [{1 100 UNASSIGNED} {2 200 UNASSIGNED} {3 300 TAKEN}]
}

// Normal usecase: no data
func ExampleListOrder_noData() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=1&limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	orders := make([]order.Order, 0)
	err := json.NewDecoder(res.Body).Decode(&orders)
	fmt.Println(err)
	fmt.Println(orders)
	// Output: 200
	// <nil>
	// []
}

// Invalid input: page or limit smaller than or equal to 0
func ExampleListOrder_wrongInput() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=0&limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Page must be greater than 0, Limit must be greater than or equal to 0"}
}

// Missing Limit Params
func ExampleListOrder_missingLimit() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=0", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Missing limit query params"}
}

// Limit Params is not number
func ExampleListOrder_wrongLimit() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=0&limit=a", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Limit must be a number"}
}

// Multi Limit Params
func ExampleListOrder_multiLimit() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=0&limit=1&limit=2", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Invalid Input"}
}

// Missing Page Params
func ExampleListOrder_missingPage() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Missing page query params"}
}

// Limit Params is not number
func ExampleListOrder_wrongPage() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=a&limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Page must be a number"}
}

// Multi Limit Params
func ExampleListOrder_multiPage() {
	orderServiceHelper = orderMock{}

	listOrderMock = func(page, limit int, filterType string) ([]order.Order, error) {
		return []order.Order{}, nil
	}
	req := httptest.NewRequest(http.MethodGet, "/orders?page=0&page=1&limit=1", nil)
	w := httptest.NewRecorder()
	ListOrders(w, req)
	res := w.Result()
	defer res.Body.Close()
	fmt.Println(res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(err)
	fmt.Println(string(body))
	// Output: 400
	// <nil>
	// {"error": "Invalid Input"}
}
