Port: 8080  
Get GOOGLE_MAP_ACCESS_TOKEN from Google Cloud Platform  
Activate Google Direction API permission and generate access token from GCP credential page  
Set the GOOGLE_MAP_ACCESS_TOKEN environment in docker-compose with your google API Token  

## Unit test
Run unit test via `go test ./...`   

## Integration test
start test db via `docker-compose -f docker-compose.test.yml up -d`  
Run integration test via `GOOGLE_MAP_ACCESS_TOKEN={{YOUR_GOOGLE_API_TOKEN}} go test ./... --tags integration`