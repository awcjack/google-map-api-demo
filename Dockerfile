FROM golang:1.18.0 as build

WORKDIR /app

COPY . /app
RUN go mod download
RUN go build /app/cmd/googleMapApiDemo/googleMapApiDemo.go

FROM gcr.io/distroless/base-debian10
EXPOSE 8080
ENV PORT 8080
COPY --from=build /app/googleMapApiDemo /

CMD ["/googleMapApiDemo"]