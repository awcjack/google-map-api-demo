# !/bin/sh

docker-compose up -d

while [ "`docker inspect -f {{.State.Health.Status}} mysql-db`" != "healthy" ]; do     sleep 2; done;
docker exec -i mysql-db mysql -uuser -ppassword db < db.sql