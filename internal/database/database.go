package database

import (
	"database/sql"
	"fmt"
	"log"

	"google-map-api-demo/internal/env"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// Init DB Pool
func initDBPool() *sql.DB {
	_db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", env.DBUsername, env.DBPassword, env.DBHostname, env.DBPort, env.DBName))

	if err != nil {
		log.Fatal(err)
	}
	_db.SetMaxOpenConns(200)
	_db.SetMaxIdleConns(10)

	db = _db

	return _db
}

// Get Global DB Pool variable or init DB Pool
func GetDBPool() *sql.DB {
	if db == nil {
		db = initDBPool()
	}
	return db
}

// Create DB Table
func InitDB(db *sql.DB) {
	result, err := db.Exec(`CREATE TABLE IF NOT EXISTS orders (
		id INT NOT NULL AUTO_INCREMENT,
		distance INT NOT NULL,
		taken BOOLEAN NOT NULL,
		PRIMARY KEY(id)
	)`)
	fmt.Println("Init result", result)
	if err != nil {
		log.Fatalln("Init DB error", err)
	}
}
