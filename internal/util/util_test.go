package util

import (
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"math"
	"testing"
)

/*
floatString means string to be check whether is float64 (including integer) format
expected means the result we expect
*/
type floatTests struct {
	floatString string
	expected    error
}

var checkFloatTests = []floatTests{
	{"0", nil},
	{"1", nil},
	{"-1", nil},
	{"", customErrors.ErrInvalidInput},
	{"abc", customErrors.ErrInvalidInput},
	{"1a", customErrors.ErrInvalidInput},
	{"a1", customErrors.ErrInvalidInput},
	{"0.1.1", customErrors.ErrInvalidInput},
	{"0.1", nil},
	{"0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001", nil},
	{"0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001", nil}, // become 0
	{"10000000000000000000", nil},
	{fmt.Sprint(math.MaxFloat64), nil},
	{fmt.Sprint(math.MaxFloat64 + 1), nil},
	{"-10000000000000000000", nil},
}

func TestCheckFloat(t *testing.T) {
	for _, test := range checkFloatTests {
		if output := CheckFloat(test.floatString); output != test.expected {
			t.Errorf("Output %q not equal to expected %q", output, test.expected)
		}
	}
}

// Normal usercase: correct float64 format
func ExampleCheckFloat() {
	fmt.Println(CheckFloat("0.1"))
	// Output: <nil>
}

// Integer input: accepted float64 format
func ExampleCheckFloat_int() {
	fmt.Println(CheckFloat("0"))
	// Output: <nil>
}

// Empty input: Error
func ExampleCheckFloat_emptyInput() {
	fmt.Println(CheckFloat(""))
	// Output: Invalid Input
}

// Character input: Error
func ExampleCheckFloat_wrongInput() {
	fmt.Println(CheckFloat("abc"))
	// Output: Invalid Input
}
