package util

import (
	"fmt"
	"net/http"
)

// Send Bat request status code and custom message
func SendBadRequest(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusBadRequest)
	if message == "" {
		message = "Invalid Input"
	}
	w.Write([]byte(fmt.Sprintf(`{"error": "%s"}`, message)))
}

// Send internal server error status code and custom message
func SendInternalServerError(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusInternalServerError)
	if message == "" {
		message = "Internal Server Error, Please try again later"
	}
	w.Write([]byte(fmt.Sprintf(`{"error": "%s"}`, message)))
}
