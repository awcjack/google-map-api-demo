package util

import (
	"google-map-api-demo/internal/customErrors"
	"strconv"
)

// Check whether input string is float number format
func CheckFloat(floatString string) error {
	if _, err := strconv.ParseFloat(floatString, 10); err != nil || floatString == "" {
		return customErrors.ErrInvalidInput
	}
	return nil
}
