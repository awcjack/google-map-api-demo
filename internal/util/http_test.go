package util

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http/httptest"
	"testing"
)

type errorResponse struct {
	Error string `json:"error"`
}

func TestSendBadRequest(t *testing.T) {
	w := httptest.NewRecorder()
	SendBadRequest(w, "")
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	var responseBody errorResponse
	err = json.Unmarshal(data, &responseBody)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if responseBody.Error != "Invalid Input" {
		t.Errorf("expected error field to be Invalid Input got %v", responseBody.Error)
	}
}

// Send Bad Request error to client
func ExampleSendBadRequest() {
	w := httptest.NewRecorder()
	SendBadRequest(w, "")
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("expected error to be nil got %v", err)
	}
	fmt.Println(string(data))
	// Output: {"error": "Invalid Input"}
}

func TestSendInternalServerError(t *testing.T) {
	w := httptest.NewRecorder()
	SendInternalServerError(w, "")
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	var responseBody errorResponse
	err = json.Unmarshal(data, &responseBody)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if responseBody.Error != "Internal Server Error, Please try again later" {
		t.Errorf("expected Error field to be Internal Server Error, Please try again later got %v", responseBody.Error)
	}
}

// Send Internal server error to client
func ExampleSendInternalServerError() {
	w := httptest.NewRecorder()
	SendInternalServerError(w, "")
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("expected error to be nil got %v", err)
	}
	fmt.Println(string(data))
	// Output: {"error": "Internal Server Error, Please try again later"}
}
