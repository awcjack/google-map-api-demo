package env

import (
	"fmt"
	"os"
	"strconv"
)

var Port int
var DBUsername string
var DBPassword string
var DBHostname string
var DBPort int
var DBName string
var GMapToken string

// Parse Environment to string
func GetenvStr(key string) string {
	v := os.Getenv(key)
	if v == "" {
		fmt.Printf("Load %s env Error %s\n", key, "getenv: environment variable empty")
		return v
	}
	return v
}

// Parse Environment to integer
func GetenvInt(key string) int {
	s := GetenvStr(key)
	v, err := strconv.Atoi(s)
	if err != nil {
		fmt.Printf("Load %s env Error %v (Parse int)\n", key, err)
		return 0
	}
	return v
}

// Parse Environment to boolean
func GetenvBool(key string) bool {
	s := GetenvStr(key)
	if s == "1" {
		return false
	}
	v, err := strconv.ParseBool(s)
	if err != nil {
		fmt.Printf("Load %s env Error %v (Parse bool)\n", key, err)
		return false
	}
	return v
}

func LoadEnv() {
	Port = GetenvInt("PORT")
	DBUsername = GetenvStr("DB_USERNAME")
	DBPassword = GetenvStr("DB_PASSWORD")
	DBHostname = GetenvStr("DB_HOSTNAME")
	DBPort = GetenvInt("DB_PORT")
	DBName = GetenvStr("DB_NAME")
	GMapToken = GetenvStr("GOOGLE_MAP_ACCESS_TOKEN")
}
