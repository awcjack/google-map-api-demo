package env

import (
	"fmt"
	"os"
	"testing"
)

/*
key means the environment variable name
expected means expected string value from environment
*/
type getenvStrTest struct {
	key, expected string
}

var getenvStrTests = []getenvStrTest{
	{"KEY", "VALUE"},
	{"MISSING_KEY", ""},
	{"", ""},
	{"KEY", "VALUE"},
	{"INT_KEY", "1"},
	{"BOOL_KEY", "true"},
}

/*
key means the environment variable name
expected means expected int value from environment
*/
type getenvIntTest struct {
	key      string
	expected int
}

var getenvIntTests = []getenvIntTest{
	{"KEY", 0},
	{"MISSING_KEY", 0},
	{"", 0},
	{"KEY", 0},
	{"INT_KEY", 1},
	{"BOOL_KEY", 0},
}

/*
key means the environment variable name
expected means expected bool value from environment
*/
type getenvBoolTest struct {
	key      string
	expected bool
}

var getenvBoolTests = []getenvBoolTest{
	{"KEY", false},
	{"MISSING_KEY", false},
	{"", false},
	{"KEY", false},
	{"INT_KEY", false},
	{"BOOL_KEY", true},
}

func TestGetenvStr(t *testing.T) {
	t.Setenv("KEY", "VALUE")
	t.Setenv("INT_KEY", "1")
	t.Setenv("BOOL_KEY", "true")
	for _, test := range getenvStrTests {
		if output := GetenvStr(test.key); output != test.expected {
			t.Errorf("Output %q not equal to expected %q", output, test.expected)
		}
	}
}

// Exist KEY-VALUE pair: return value
func ExampleGetenvStr() {
	os.Setenv("KEY", "VALUE")
	fmt.Println(GetenvStr("KEY"))
	// Output: VALUE
}

// Exist KEY-VALUE pair with integer value: return string value
func ExampleGetenvStr_intValue() {
	os.Setenv("KEY", "1")
	fmt.Println(GetenvStr("KEY"))
	// Output: 1
}

// Exist KEY-VALUE pair with bool value: return string value
func ExampleGetenvStr_boolValue() {
	os.Setenv("KEY", "true")
	fmt.Println(GetenvStr("KEY"))
	// true
}

// Missing Key: return empty string
func ExampleGetenvStr_missingValue() {
	fmt.Println(GetenvStr("MISSING_KEY"))
	// Output: Load MISSING_KEY env Error getenv: environment variable empty
}

func TestGetenvInt(t *testing.T) {
	t.Setenv("KEY", "VALUE")
	t.Setenv("INT_KEY", "1")
	t.Setenv("BOOL_KEY", "true")
	for _, test := range getenvIntTests {
		if output := GetenvInt(test.key); output != test.expected {
			t.Errorf("Output %q not equal to expected %q", output, test.expected)
		}
	}
}

// Exist KEY-VALUE pair with integer value: return int
func ExampleGetenvInt() {
	os.Setenv("KEY", "1")
	fmt.Println(GetenvInt("KEY"))
	// Output: 1
}

// Exist KEY-VALUE pair with bool value: return 0
func ExampleGetenvInt_boolValue() {
	os.Setenv("KEY", "true")
	fmt.Println(GetenvInt("KEY"))
	// 0
}

// Exist KEY-VALUE pair with string value: return 0
func ExampleGetenvInt_stringValue() {
	os.Setenv("KEY", "VALUE")
	fmt.Println(GetenvInt("KEY"))
	// Output: Load KEY env Error strconv.Atoi: parsing "VALUE": invalid syntax (Parse int)
	// 0
}

// Missing KEY: return 0
func ExampleGetenvInt_missingValue() {
	fmt.Println(GetenvInt("MISSING_KEY"))
	// Output: Load MISSING_KEY env Error getenv: environment variable empty
	// Load MISSING_KEY env Error strconv.Atoi: parsing "": invalid syntax (Parse int)
	// 0
}

func TestGetenvBool(t *testing.T) {
	t.Setenv("KEY", "VALUE")
	t.Setenv("INT_KEY", "1")
	t.Setenv("BOOL_KEY", "true")
	for _, test := range getenvBoolTests {
		if output := GetenvBool(test.key); output != test.expected {
			t.Errorf("Output %v %v not equal to expected %v", test.key, output, test.expected)
		}
	}
}

// Exist KEY-VALUE pair with bool value: return bool
func ExampleGetenvBool() {
	os.Setenv("KEY", "true")
	fmt.Println(GetenvBool("KEY"))
	// Output: true
}

// Exist KEY-VALUE pair with integer value: return false
func ExampleGetenvBool_intValue() {
	os.Setenv("KEY", "1")
	fmt.Println(GetenvBool("KEY"))
	// Output: false
}

// Exist KEY-VALUE pair with string value: return false
func ExampleGetenvBool_stringValue() {
	os.Setenv("KEY", "VALUE")
	fmt.Println(GetenvBool("KEY"))
	// Output: Load KEY env Error strconv.ParseBool: parsing "VALUE": invalid syntax (Parse bool)
	// false
}

// Missing KEY: return false
func ExampleGetenvBool_missingValue() {
	fmt.Println(GetenvBool("MISSING_KEY"))
	// Output: Load MISSING_KEY env Error getenv: environment variable empty
	// Load MISSING_KEY env Error strconv.ParseBool: parsing "": invalid syntax (Parse bool)
	// false
}
