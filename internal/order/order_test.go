package order

import (
	"database/sql"
	"errors"
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/mock"
)

type googleMapMock struct {
	mock.Mock
}

var getDistanceFromGMap func(orig, dest string) (int, error)

func (gMap googleMapMock) GetDistanceFromGMap(orig, dest string) (int, error) {
	return getDistanceFromGMap(orig, dest)
}

type dbMock struct {
	mock.Mock
}

var getDBPool func() *sql.DB

func (dbPool dbMock) GetDBPool() *sql.DB {
	return getDBPool()
}

func TestCreateOrder(t *testing.T) {
	googleMapHelper = googleMapMock{}
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}

	_, err = CreateOrder("a", "1", "1", "1")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error")
	}

	_, err = CreateOrder("1", "a", "1", "1")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error")
	}

	_, err = CreateOrder("1", "1", "a", "1")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error")
	}

	_, err = CreateOrder("1", "1", "1", "a")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error")
	}

	getDistanceFromGMap = func(orig, dest string) (int, error) {
		return 0, errors.New("Missing Info")
	}
	_, err = CreateOrder("1", "1", "1", "1")
	if err == nil {
		t.Fatal("Expecting missing info error")
	}

	getDistanceFromGMap = func(orig, dest string) (int, error) {
		return 100, nil
	}
	mock.ExpectExec("INSERT INTO orders \\(distance,taken\\) VALUES \\(.*, false\\)").WithArgs(100).WillReturnResult(sqlmock.NewResult(1, 0))
	order, err := CreateOrder("1", "1", "1", "1")

	if err != nil {
		t.Fatal("Not Expecting missing info error")
	}

	if order.Id != 1 {
		t.Fatal(fmt.Sprintf("Expecting orderId is 1 but returned %v", order.Id))
	}

	if order.Distance != 100 {
		t.Fatal(fmt.Sprintf("Expecting distance is 100 but returned %v", order.Distance))
	}

	if order.Status != "UNASSIGNED" {
		t.Fatal(fmt.Sprintf("Expecting status is unassigned but returned %v", order.Status))
	}
}

// Normal case: db return id 1 and distance return 100
func ExampleCreateOrder() {
	googleMapHelper = googleMapMock{}
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}

	getDistanceFromGMap = func(orig, dest string) (int, error) {
		return 100, nil
	}
	mock.ExpectExec("INSERT INTO orders \\(distance,taken\\) VALUES \\(.*, false\\)").WithArgs(100).WillReturnResult(sqlmock.NewResult(1, 0))

	order, err := CreateOrder("1", "1", "1", "1")
	fmt.Println(err)
	fmt.Println(order)
	// Output: <nil>
	// {1 100 UNASSIGNED}
}

// Invalid input: in start/destination lat/long
func ExampleCreateOrder_wrongInput() {
	_, err := CreateOrder("a", "1", "1", "1")
	fmt.Println(err)
	// Output: Invalid Input
}

// No Route from google map API
func ExampleCreateOrder_noRoute() {
	googleMapHelper = googleMapMock{}

	getDistanceFromGMap = func(orig, dest string) (int, error) {
		return 0, customErrors.ErrNoRoute
	}
	_, err := CreateOrder("1", "1", "1", "1")
	fmt.Println(err)
	// Output: No Route Found
}

func TestTakeOrder(t *testing.T) {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}

	mock.ExpectExec("UPDATE orders SET taken = true WHERE id = .* AND taken = false").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 0))
	err = TakeOrder(1)
	if !errors.Is(err, customErrors.ErrInvalidOrder) {
		t.Fatalf("Expecting returning Invalid Order Error")
	}

	// Never happen due to primary key
	mock.ExpectExec("UPDATE orders SET taken = true WHERE id = .* AND taken = false").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 2))
	err = TakeOrder(1)
	if !errors.Is(err, customErrors.ErrInternalServerError) {
		t.Fatalf("Expecting returning Internal server Error")
	}

	mock.ExpectExec("UPDATE orders SET taken = true WHERE id = .* AND taken = false").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))
	err = TakeOrder(1)
	if err != nil {
		t.Fatalf("Not Expecting Error")
	}
}

// Normal use-case: picking order still not assigned
func ExampleTakeOrder() {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}
	mock.ExpectExec("UPDATE orders SET taken = true WHERE id = .* AND taken = false").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))
	err = TakeOrder(1)
	if err != nil {
		fmt.Println("Not Expecting Error")
	}
	fmt.Println(err)
	// Output: <nil>
}

// Error: Order Taken by others already
func ExampleTakeOrder_taken() {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}
	mock.ExpectExec("UPDATE orders SET taken = true WHERE id = .* AND taken = false").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 0))
	err = TakeOrder(1)
	fmt.Println(err)
	// Output: Invalid Order
}

func TestListOrder(t *testing.T) {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}

	orders, err := ListOrder(1, 0, "")
	if len(orders) != 0 {
		t.Fatalf("Limit 0 should return empty Array")
	}

	rows := sqlmock.NewRows([]string{"id", "distance", "taken"})
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err = ListOrder(1, 10, "")
	if err != nil {
		t.Fatalf("Not expecting error")
	}
	if len(orders) != 0 {
		t.Fatalf("Expect no row")
	}

	rows = sqlmock.NewRows([]string{"id", "distance", "taken"}).AddRow(1, 100, false)
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err = ListOrder(1, 10, "")
	if err != nil {
		t.Fatalf("Not expecting error")
	}
	if len(orders) != 1 {
		t.Fatalf("Expect only one row")
	}

	rows = sqlmock.NewRows([]string{"id", "distance", "taken"}).
		AddRow(1, 100, false).
		AddRow(2, 200, false).
		AddRow(3, 300, false).
		AddRow(4, 400, false).
		AddRow(5, 500, false).
		AddRow(6, 600, false).
		AddRow(7, 700, false).
		AddRow(8, 800, false).
		AddRow(9, 900, false).
		AddRow(10, 1000, false)
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err = ListOrder(1, 10, "")
	if err != nil {
		t.Fatalf("Not expecting error")
	}
	if len(orders) != 10 {
		t.Fatalf("Expect only ten row %v", orders)
	}

	rows = sqlmock.NewRows([]string{"id", "distance", "taken"}).
		AddRow(2, 200, false).
		AddRow(3, 300, false).
		AddRow(4, 400, false).
		AddRow(5, 500, false).
		AddRow(6, 600, false).
		AddRow(7, 700, false).
		AddRow(8, 800, false).
		AddRow(9, 900, false).
		AddRow(10, 1000, false).
		AddRow(1, 100, true)
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err = ListOrder(1, 10, "")
	if err != nil {
		t.Fatalf("Not expecting error")
	}
	if len(orders) != 10 {
		t.Fatalf("Expect only ten row %v", orders)
	}
	if orders[0].Id != 2 {
		t.Fatalf("Expect sort by taken status and then id")
	}
}

// Normal use-case: get page id first 10 order
func ExampleListOrder() {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}
	rows := sqlmock.NewRows([]string{"id", "distance", "taken"}).
		AddRow(1, 100, false).
		AddRow(2, 200, false).
		AddRow(3, 300, false).
		AddRow(4, 400, false).
		AddRow(5, 500, false).
		AddRow(6, 600, false).
		AddRow(7, 700, false).
		AddRow(8, 800, false).
		AddRow(9, 900, false).
		AddRow(10, 1000, false)
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err := ListOrder(1, 10, "")
	if err != nil {
		fmt.Printf("Not expecting error")
	}
	fmt.Println(orders)
	// Output: [{1 100 UNASSIGNED} {2 200 UNASSIGNED} {3 300 UNASSIGNED} {4 400 UNASSIGNED} {5 500 UNASSIGNED} {6 600 UNASSIGNED} {7 700 UNASSIGNED} {8 800 UNASSIGNED} {9 900 UNASSIGNED} {10 1000 UNASSIGNED}]
}

// Normal use-case: get page id first 10 order sort by taken and then sort by id
func ExampleListOrder_sort() {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}
	rows := sqlmock.NewRows([]string{"id", "distance", "taken"}).
		AddRow(2, 200, false).
		AddRow(3, 300, false).
		AddRow(4, 400, false).
		AddRow(5, 500, false).
		AddRow(6, 600, false).
		AddRow(7, 700, false).
		AddRow(8, 800, false).
		AddRow(9, 900, false).
		AddRow(10, 1000, false).
		AddRow(1, 100, true)
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, err := ListOrder(1, 10, "")
	if err != nil {
		fmt.Printf("Not expecting error")
	}
	fmt.Println(orders)
	// Output: [{2 200 UNASSIGNED} {3 300 UNASSIGNED} {4 400 UNASSIGNED} {5 500 UNASSIGNED} {6 600 UNASSIGNED} {7 700 UNASSIGNED} {8 800 UNASSIGNED} {9 900 UNASSIGNED} {10 1000 UNASSIGNED} {1 100 TAKEN}]
}

// 0 Limit always return empty array
func ExampleListOrder_zeroLimit() {
	orders, _ := ListOrder(1, 0, "")
	fmt.Println(orders)
	// Output: []
}

// Normal use-case: No order in db yet
func ExampleListOrder_noOrder() {
	dbHelper = dbMock{}

	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	getDBPool = func() *sql.DB {
		return db
	}
	rows := sqlmock.NewRows([]string{"id", "distance", "taken"})
	mock.ExpectQuery("SELECT id, distance, taken from orders .*ORDER BY taken ASC, id ASC LIMIT ?,?").WithArgs(0, 10).WillReturnRows(rows)
	orders, _ := ListOrder(1, 10, "")
	fmt.Println(orders)
	// Output: []
}
