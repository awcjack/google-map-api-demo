package order

import (
	"database/sql"
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"google-map-api-demo/internal/database"
	"google-map-api-demo/internal/googleMap"
	"google-map-api-demo/internal/util"
)

// interface for mocking
type googleMapInterface interface {
	GetDistanceFromGMap(orig, dest string) (int, error)
}

type googleMapStruct struct{}

func (gMap googleMapStruct) GetDistanceFromGMap(orig, dest string) (int, error) {
	return googleMap.GetDistanceFromGMap(orig, dest)
}

var googleMapHelper googleMapInterface = googleMapStruct{}

type dbInterface interface {
	GetDBPool() *sql.DB
}

type dbStruct struct{}

func (dbPool dbStruct) GetDBPool() *sql.DB {
	return database.GetDBPool()
}

var dbHelper dbInterface = dbStruct{}

type Order struct {
	Id       int    `json:"id" db:"telcode"`
	Distance int    `json:"distance" db:"distance"`
	Status   string `json:"status"`
}

// Handler of Create Order API
func CreateOrder(startLat, startLong, endLat, endLong string) (Order, error) {
	err := util.CheckFloat(startLat)
	if err != nil {
		return Order{}, err
	}
	err = util.CheckFloat(startLong)
	if err != nil {
		return Order{}, err
	}
	err = util.CheckFloat(endLat)
	if err != nil {
		return Order{}, err
	}
	err = util.CheckFloat(endLong)
	if err != nil {
		return Order{}, err
	}

	distance, err := googleMapHelper.GetDistanceFromGMap(fmt.Sprintf("%s,%s", startLat, startLong), fmt.Sprintf("%s,%s", endLat, endLong))
	if err != nil {
		return Order{}, err
	}

	db := dbHelper.GetDBPool()
	result, err := db.Exec("INSERT INTO orders (distance,taken) VALUES (?, false)", distance)
	if err != nil {
		return Order{}, err
	}
	orderId, err := result.LastInsertId()
	if err != nil {
		return Order{}, err
	}

	return Order{
		Id:       int(orderId),
		Distance: distance,
		Status:   "UNASSIGNED",
	}, nil
}

// Handler of Take Order API
func TakeOrder(orderId int) error {
	db := dbHelper.GetDBPool()
	result, err := db.Exec("UPDATE orders SET taken = true WHERE id = ? AND taken = false", orderId)
	if err != nil {
		return err
	}

	updatedCount, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if updatedCount == 0 {
		return customErrors.ErrInvalidOrder
	}
	if updatedCount > 1 {
		return customErrors.ErrInternalServerError
	}
	return nil
}

// Handler of List Order API
func ListOrder(page, limit int, filterType string) ([]Order, error) {
	if limit == 0 {
		return []Order{}, nil
	}

	db := dbHelper.GetDBPool()
	var rows *sql.Rows
	var err error
	if filterType == "TAKEN" {
		rows, err = db.Query("SELECT id, distance, taken from orders WHERE taken = true ORDER BY taken ASC, id ASC LIMIT ?,?", (page-1)*limit, limit)
	} else if filterType == "UNASSIGNED" {
		rows, err = db.Query("SELECT id, distance, taken from orders WHERE taken = false ORDER BY taken ASC, id ASC LIMIT ?,?", (page-1)*limit, limit)
	} else {
		rows, err = db.Query("SELECT id, distance, taken from orders ORDER BY taken ASC, id ASC LIMIT ?,?", (page-1)*limit, limit)
	}

	if err != nil {
		return []Order{}, err
	}

	defer rows.Close()

	orders := make([]Order, 0, limit)
	for rows.Next() {
		var order Order
		var taken bool

		if err := rows.Scan(&order.Id, &order.Distance, &taken); err != nil {
			continue
		}

		if taken {
			order.Status = "TAKEN"
		} else {
			order.Status = "UNASSIGNED"
		}
		orders = append(orders, order)
	}

	return orders, nil
}
