package httpServer

import (
	orderControllers "google-map-api-demo/controllers/order"

	"github.com/gorilla/mux"
)

// Setup 3 API
func SetupHandler(router *mux.Router) {
	router.HandleFunc("/orders", orderControllers.PlaceOrder).Methods("POST")
	router.HandleFunc("/orders/{orderId}", orderControllers.TakeOrder).Methods("PATCH")
	router.HandleFunc("/orders", orderControllers.ListOrders).Methods("GET")
}
