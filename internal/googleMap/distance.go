package googleMap

import (
	"encoding/json"
	"errors"
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"google-map-api-demo/internal/env"
	"io/ioutil"
	"net/http"
	"time"
)

// interface for mocking
type gMapClientInterface interface {
	Directions(orig, dest string) ([]Route, error)
}

type gMapClient struct {
}

// Route represents a single route between an origin and a destination.
type Route struct {
	// Legs contains information about a leg of the route, between two locations within
	// the given route. A separate leg will be present for each waypoint or destination
	// specified. A route with no waypoints will contain exactly one leg within the legs
	// array.
	Legs []*Leg `json:"legs"`
	// Warnings contains an array of warnings to be displayed when showing these
	// directions. You must handle and display these warnings yourself.
	Warnings []string `json:"warnings"`
}

// Fare represents the total fare for a route.
type Fare struct {
	// Currency is an ISO 4217 currency code indicating the currency that the amount
	// is expressed in.
	Currency string `json:"currency"`

	// Value is the total fare amount, in the currency specified above.
	Value float64 `json:"value"`

	// Text is the total fare amount, formatted in the requested language.
	Text string `json:"text"`
}

// Leg represents a single leg of a route.
type Leg struct {
	// Distance indicates the total distance covered by this leg.
	Distance `json:"distance"`
}

// Step represents a single step of a leg.
type Step struct {
	// Distance contains the distance covered by this step until the next step.
	Distance `json:"distance"`
}

// Distance is the API representation for a distance between two points.
type Distance struct {
	// Meters is the numeric distance, always in meters. This is intended to be used
	// only in algorithmic situations, e.g. sorting results by some user specified
	// metric.
	Meters int `json:"value"`
}

// Call Google Map API to get distance (Not using Library due to weird error in library)
func (g gMapClient) Directions(orig, dest string) ([]Route, error) {
	if env.GMapToken == "" {
		return []Route{}, customErrors.ErrMissiongToken
	}

	resp, err := http.Get(fmt.Sprintf("https://maps.googleapis.com/maps/api/directions/json?origin=%s&mode=driving&traffic_model=best_guess&destination=%s&departure_time=%d&key=%s", orig, dest, time.Now().Unix(), env.GMapToken))
	if err != nil {
		return []Route{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []Route{}, err
	}
	var response struct {
		Routes []Route `json:"routes"`
		Status string  `json:"status"`
		Error  string  `json:"error_message"`
	}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return []Route{}, err
	}
	if response.Status == "NOT_FOUND" {
		return []Route{}, nil
	}
	if response.Status != "OK" {
		return []Route{}, errors.New(response.Error)
	}
	return response.Routes, nil
}

var gMapClientWrapper gMapClientInterface = gMapClient{}

// Get Distance from Directions function
func GetDistanceFromGMap(orig, dest string) (int, error) {
	routes, err := gMapClientWrapper.Directions(orig, dest)
	if err != nil {
		return 0, err
	}

	if len(routes) == 0 {
		return 0, customErrors.ErrNoRoute
	}

	bestRoute := routes[0]
	if bestRoute.Legs != nil && len(bestRoute.Legs) > 0 {
		if bestRoute.Legs[0].Distance.Meters != 0 {
			return bestRoute.Legs[0].Distance.Meters, nil
		}
		return 0, customErrors.ErrInvalidInput
	}
	return 0, customErrors.ErrInternalServerError
}
