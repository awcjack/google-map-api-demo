package googleMap

import (
	"errors"
	"fmt"
	"google-map-api-demo/internal/customErrors"
	"google-map-api-demo/internal/env"
	"testing"

	"github.com/stretchr/testify/mock"
	"googlemaps.github.io/maps"
)

type gMapClientMock struct {
	client *maps.Client
	mock.Mock
}

var directionsMock func(orig, dest string) ([]Route, error)

func (g gMapClientMock) Directions(orig, dest string) ([]Route, error) {
	return directionsMock(orig, dest)
}

func TestDirections(t *testing.T) {
	orgiToken := env.GMapToken
	env.GMapToken = ""
	_, err := gMapClientWrapper.Directions("1,1", "1,1")
	if !errors.Is(err, customErrors.ErrMissiongToken) {
		t.Fatalf("Expecting throwing Missing Token error")
	}
	env.GMapToken = orgiToken
}

func TestGetDistanceFromGMap(t *testing.T) {
	gMapClientWrapper = gMapClientMock{}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return nil, errors.New("Missing Info")
	}
	distance, err := GetDistanceFromGMap("0,0", "0,0")
	if err == nil || distance != 0 {
		t.Fatal("Expecting throwing error from GetDistanceFromGMap")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if !errors.Is(err, customErrors.ErrNoRoute) {
		t.Fatal("Expecting throwing Invalid Input error from GetDistanceFromGMap If cannot get route")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: nil}}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if !errors.Is(err, customErrors.ErrInternalServerError) {
		t.Fatal("Expecting throwing No Route error from GetDistanceFromGMap If can get route but missing Legs")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{}}}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if !errors.Is(err, customErrors.ErrInternalServerError) {
		t.Fatal("Expecting throwing Internal Server error from GetDistanceFromGMap If can get route but missing Legs")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{{Distance: Distance{}}}}}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error from GetDistanceFromGMap If can get route but missing Distance")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{{Distance: Distance{Meters: 0}}}}}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if !errors.Is(err, customErrors.ErrInvalidInput) {
		t.Fatal("Expecting throwing Invalid Input error from GetDistanceFromGMap Distance is 0 meter")
	}

	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{{Distance: Distance{Meters: 10}}}}}, nil
	}
	distance, err = GetDistanceFromGMap("0,0", "0,0")
	if err != nil {
		t.Fatal("Expecting not throwing error from GetDistanceFromGMap Distance is not 0 meter")
	}
}

// Correct response from Google Map API
func ExampleGetDistanceFromGMap() {
	gMapClientWrapper = gMapClientMock{}
	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{{Distance: Distance{Meters: 10}}}}}, nil
	}
	distance, err := GetDistanceFromGMap("0,0", "0,0")
	if err != nil {
		fmt.Println("Expecting not throwing error from GetDistanceFromGMap Distance is not 0 meter")
	}
	fmt.Println(distance)
	// Output: 10
}

// No route found from Google Map API
func ExampleGetDistanceFromGMap_noRoute() {
	gMapClientWrapper = gMapClientMock{}
	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{}, nil
	}
	distance, err := GetDistanceFromGMap("0,0", "0,0")
	fmt.Println(err)
	fmt.Println(distance)
	// Output: No Route Found
	// 0
}

// Missing info
func ExampleGetDistanceFromGMap_missingInfo() {
	gMapClientWrapper = gMapClientMock{}
	directionsMock = func(orig, dest string) ([]Route, error) {
		return nil, errors.New("Missing Info")
	}
	distance, err := GetDistanceFromGMap("0,0", "0,0")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(distance)
	// Output: Missing Info
	// 0
}

// Missing info from Google Map API
func ExampleGetDistanceFromGMap_routeMissingInfo() {
	gMapClientWrapper = gMapClientMock{}
	directionsMock = func(orig, dest string) ([]Route, error) {
		return []Route{{Legs: []*Leg{}}}, nil
	}
	distance, err := GetDistanceFromGMap("0,0", "0,0")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(distance)
	// Output: Internal Server Error
	// 0
}
