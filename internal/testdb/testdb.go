//go:build integration
// +build integration

package testdb

import (
	"database/sql"
	"google-map-api-demo/internal/order"
)

// Integration test default data
func SeedOrders(db *sql.DB) error {
	orders := []order.Order{
		{
			Id:       1,
			Distance: 100,
			Status:   "UNASSIGNED",
		},
		{
			Id:       2,
			Distance: 200,
			Status:   "UNASSIGNED",
		},
		{
			Id:       3,
			Distance: 300,
			Status:   "TAKEN",
		},
	}

	for i, order := range orders {
		stmt, err := db.Prepare("INSERT INTO orders(id, distance, taken) VALUES (?, ?, ?)")
		if err != nil {
			return err
		}

		var taken = false
		if order.Status == "TAKEN" {
			taken = true
		}

		_, err = stmt.Exec(orders[i].Id, orders[i].Distance, taken)
		if err != nil {
			return err
		}
	}
	return nil
}

// Integration test clear data
func CleanOrders(db *sql.DB) error {
	_, err := db.Exec("TRUNCATE table orders")
	if err != nil {
		return err
	}
	return nil
}
