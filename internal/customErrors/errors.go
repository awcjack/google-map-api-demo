package customErrors

import "errors"

var ErrMissiongToken = errors.New("Missing Google Map Token")
var ErrNoRoute = errors.New("No Route Found")
var ErrInternalServerError = errors.New("Internal Server Error")
var ErrInvalidInput = errors.New("Invalid Input")
var ErrInvalidOrder = errors.New("Invalid Order")
var ErrTakenOrder = errors.New("Order taken")
